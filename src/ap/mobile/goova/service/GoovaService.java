package ap.mobile.goova.service;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import ap.mobile.goova.base.Position;
import ap.mobile.goova.collectionservice.Session;

public class GoovaService extends Service implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener {

	private IBinder iBinder;
	private GoovaServiceInterface goovaServiceInterface;
	private LocationRequest locationRequest;
	private LocationClient locationClient;
	private Boolean isLoggingRun = false;
	
	private static int UPDATE_INTERVAL = 1000;
	private static int FASTEST_INTERVAL = 500;
	
	private Session session;
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d("goova","onStartCommand");
		return Service.START_NOT_STICKY;
	}
	
	@Override
	public IBinder onBind(Intent arg0) {
		iBinder = new GoovaServiceBinder(this);
		this.locationRequest = LocationRequest.create();
		this.locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		this.locationRequest.setInterval(UPDATE_INTERVAL);
		this.locationRequest.setFastestInterval(FASTEST_INTERVAL);
		this.locationClient = new LocationClient(getApplicationContext(), this, this);
		this.locationClient.connect();
		Log.d("goova","onBind");
		return iBinder;
	}
	
	@Override
	public boolean onUnbind(Intent intent) {
		this.locationClient.disconnect();
		return super.onUnbind(intent);
	}
	
	public void startPositionLogging() {
		if(this.session == null) {
			this.session = new Session();
			this.session.resume();
		}
		Log.d("goova","startPositionLogging");
		this.locationClient.requestLocationUpdates(this.locationRequest, this);
		this.isLoggingRun = true;
	}
	
	public void stopPositionLogging() {
		Log.d("goova","stopPositionLogging");
		this.locationClient.removeLocationUpdates(this);
		this.isLoggingRun = false;
	}
	
	public Boolean isLoggingRun() {
		return this.isLoggingRun;
	}
	
	public Boolean isLocationServiceConnected() {
		return this.locationClient.isConnected();
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		Log.d("goova", "connection fail");
	}

	@Override
	public void onConnected(Bundle arg0) {
		Log.d("goova", "client connected");
		if(this.goovaServiceInterface != null)
		this.goovaServiceInterface.onLocationServiceConnected();
	}

	@Override
	public void onDisconnected() {
		Log.d("goova", "client disconnected");
	}

	public void getCurrentLocation() {
		if(this.locationClient != null && this.locationClient.isConnected()) {
			Location location = this.locationClient.getLastLocation();
			if(this.goovaServiceInterface != null)
				this.goovaServiceInterface.onLocationUpdate(location);
		} else Log.d("goova","client location is not connected");
	}

	public void setClient(GoovaServiceInterface goovaServiceInterface) {
		this.goovaServiceInterface = goovaServiceInterface;
	}

	@Override
	public void onLocationChanged(Location location) {
		if(this.session.isRunning())
		{
			Position position = new Position(location);
			this.session.addPosition(position);
		}
		if(this.goovaServiceInterface != null)
			this.goovaServiceInterface.onLocationUpdate(location);
	}
	
	public Session getSession() {
		return this.session;
	}

}
