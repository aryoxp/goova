package ap.mobile.goova.service;

import android.location.Location;

public interface GoovaServiceInterface {
	public void onServiceConnected(GoovaService service);
	public void onLocationServiceConnected();
	public void onLocationUpdate(Location location);
}
