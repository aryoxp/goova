package ap.mobile.goova.fragments;

import java.util.ArrayList;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import ap.mobile.goova.R;
import ap.mobile.goova.base.Position;

public class SessionMapFragment extends Fragment implements OnMapLoadedCallback {
	
	private View v;
	private ArrayList<Position> positions;
	private GoogleMap map;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		this.v = inflater.inflate(R.layout.fragment_map, null);
		this.map = ((SupportMapFragment) getFragmentManager().findFragmentById(R.id.sessionMap)).getMap();
		this.map.setOnMapLoadedCallback(this);
		return this.v;
	}
	
	public void setPositions(ArrayList<Position> positions) {
		this.positions = positions;
	}
		
	public static SessionMapFragment newInstance(ArrayList<Position> positions) {
		SessionMapFragment sessionMapFragments = new SessionMapFragment();
		sessionMapFragments.setPositions(positions);
		return sessionMapFragments;
	}

	@Override
	public void onMapLoaded() {
		if(this.positions != null) 
		{
			LatLngBounds.Builder bc = new LatLngBounds.Builder();

			for(int i = 1; i<this.positions.size(); i++) {
				Position a = this.positions.get(i-1);
				Position b = this.positions.get(i);
				LatLng aLatLng = new LatLng(a.getLatitude(), a.getLongitude());
				LatLng bLatLng = new LatLng(b.getLatitude(), b.getLongitude());
				this.map.addPolyline(new PolylineOptions()
			    .add(aLatLng, bLatLng)
			    .width(5)
			    .color(Color.RED));
				bc.include(bLatLng);
			}
			map.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));
		}
	}
}
