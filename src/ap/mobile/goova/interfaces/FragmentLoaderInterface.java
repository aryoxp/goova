package ap.mobile.goova.interfaces;

import android.support.v4.app.Fragment;

public interface FragmentLoaderInterface {
	public void loadFragments(Fragment fragment, Boolean addToBackStack);
}
