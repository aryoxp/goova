package ap.mobile.goova.fragments;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import ap.mobile.goova.R;
import ap.mobile.goova.collectionservice.Session;
import ap.mobile.goova.interfaces.FragmentLoaderInterface;
import ap.mobile.goova.service.GoovaService;
import ap.mobile.goova.service.GoovaServiceConnection;
import ap.mobile.goova.service.GoovaServiceInterface;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceholderFragment extends Fragment implements GoovaServiceInterface, OnClickListener, OnMapLoadedCallback {

	private GoovaService service;
	private GoovaServiceConnection serviceConnection;
	
	private GoogleMap map;
	private ImageView okButton;
	private TextView debugText;
	private Marker currentLocationMarker;
	private FragmentLoaderInterface fragmentLoaderInterface;
	private LatLng currentLocation;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        this.okButton = (ImageView) rootView.findViewById(R.id.button_record);
        this.okButton.setOnClickListener(this);
        this.debugText = (TextView) rootView.findViewById(R.id.debugText);
        rootView.findViewById(R.id.buttonDebug).setOnClickListener(this);
        FragmentManager mapFragment = getFragmentManager();
        this.map = ((SupportMapFragment) mapFragment.findFragmentById(R.id.map)).getMap();
        this.map.setMyLocationEnabled(true);
        this.map.setOnMapLoadedCallback(this);
        return rootView;
    }
    
    @Override
    public void onStart() {
    	super.onStart();
    	Intent serviceIntent = new Intent(this.getActivity().getApplicationContext(), GoovaService.class);
        this.serviceConnection = new GoovaServiceConnection(this);
        this.getActivity().bindService(serviceIntent, this.serviceConnection, Context.BIND_AUTO_CREATE);
    }
    
    @Override
    public void onStop() {
    	super.onStop();
    	if(this.serviceConnection.isConnected())
    		this.getActivity().unbindService(this.serviceConnection);
    }

	@Override
	public void onServiceConnected(GoovaService service) {
		this.service = service;
		this.service.setClient(this);
		if(this.service.isLocationServiceConnected())
			this.service.getCurrentLocation();
	}

	@Override
	public void onClick(View view) {
		switch(view.getId()) {
		case R.id.button_record:
			if(this.serviceConnection.isConnected()) {
				if(!service.isLoggingRun()) {
					this.service.startPositionLogging();
					this.okButton.setImageResource(R.drawable.bt_record_play);
				} else {
					this.service.stopPositionLogging();
					this.okButton.setImageResource(R.drawable.bt_record_start);
				}
			}
			break;
		case R.id.buttonDebug:
			if(this.service != null) {
				//String text = "";
				Session session = this.service.getSession();
				SessionMapFragment sessionMapFragment = SessionMapFragment.newInstance(session.getPositionLog());
				if(this.fragmentLoaderInterface != null) {
					fragmentLoaderInterface.loadFragments(sessionMapFragment, true);
				}
				/*
				try {
					ArrayList<Position> positions = session.getPositionLog();
					for(Position p:positions) {
						text += p.getAltitude() + ",";
					}
					Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
				} catch(Exception e) {
					
				}
				*/
			}
			break;
		}
	}

	@Override
	public void onLocationUpdate(Location location) {
		this.debugText.setText("Lat: " + location.getLatitude() + " Long: " + location.getLongitude());
		this.currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
		
		if(this.currentLocationMarker == null) {
			this.currentLocationMarker = map.addMarker(new MarkerOptions().position(currentLocation));
			this.currentLocationMarker.remove();
			this.map.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 10));
		} else this.currentLocationMarker.setPosition(currentLocation);
		
	}

	@Override
	public void onLocationServiceConnected() {
		this.service.getCurrentLocation();
	}

	public void setActivityInterface(FragmentLoaderInterface mainActivity) {
		this.fragmentLoaderInterface = mainActivity;
	}

	@Override
	public void onMapLoaded() {
        this.map.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
	}
	
}