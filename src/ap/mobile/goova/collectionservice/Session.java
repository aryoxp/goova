package ap.mobile.goova.collectionservice;

import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import ap.mobile.goova.base.Position;

public class Session {

	private Long timeStart = 0L;
	private Long timeSpan = 0L;
	
	private String name;
	private ArrayList<Position> positions;
	private Boolean isRunning = false;
	
	public Session() {
		this.name = "";
		this.positions = new ArrayList<Position>();
	}
	
	public void pause() {
		Long diff = System.currentTimeMillis() - this.timeStart;
		this.timeSpan += diff;
		this.isRunning = false;
	}
	
	public void resume() {
		this.timeStart = System.currentTimeMillis();
		this.isRunning = true;
	}
	
	public void setSessionName(String name) {
		this.name = name;
	}
	
	public String getSessionName() {
		return this.name;
	}
	
	public Long getTimeSpan() {
		return this.timeSpan;
	}
	
	public String getTimeString() {
		return String.format(Locale.US, "%d min, %d sec", 
			    TimeUnit.MILLISECONDS.toMinutes(this.timeSpan),
			    TimeUnit.MILLISECONDS.toSeconds(this.timeSpan) - 
			    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(this.timeSpan))
			);
	}
	
	public void addPosition(Position position) {
		this.positions.add(position);
	}
	
	public ArrayList<Position> getPositionLog() {
		return this.positions;
	}
	
	public Boolean isRunning() {
		return this.isRunning;
	}
	
}
