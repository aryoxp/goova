package ap.mobile.goova.service;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

public class GoovaServiceConnection implements ServiceConnection {

	private Boolean isConnected = false;
	private GoovaService service;
	private GoovaServiceInterface serviceInterface;
	
	public GoovaServiceConnection(GoovaServiceInterface serviceInterface) {
		this.serviceInterface = serviceInterface;
	}
	
	@Override
	public void onServiceConnected(ComponentName name, IBinder service) {
		GoovaServiceBinder binder = (GoovaServiceBinder) service;
		this.service = binder.getService();
		this.serviceInterface.onServiceConnected(this.service);
		this.isConnected = true;
		Log.d("goova","onServiceConnected");
	}

	@Override
	public void onServiceDisconnected(ComponentName name) {
		this.isConnected = false;
		Log.d("goova","onServiceDisconnected");
	}
	
	public GoovaService getService() {
		return this.service;
	}
	
	public Boolean isConnected() {
		return this.isConnected;
	}

}
