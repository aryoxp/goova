package ap.mobile.goova.service;

import android.os.Binder;

public class GoovaServiceBinder extends Binder {
	
	private GoovaService service;
	
	public GoovaServiceBinder(GoovaService service) {
		this.service = service;
	}
	
	public GoovaService getService() {
		return service;
	}
}
