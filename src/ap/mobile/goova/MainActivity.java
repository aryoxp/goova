package ap.mobile.goova;

import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import ap.mobile.goova.fragments.PlaceholderFragment;
import ap.mobile.goova.interfaces.FragmentLoaderInterface;

public class MainActivity extends ActionBarActivity implements FragmentLoaderInterface {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        PlaceholderFragment placeholderFragment = new PlaceholderFragment();
        placeholderFragment.setActivityInterface(this);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, placeholderFragment)
                    .commit();
        }
    }

    @Override
    protected void onResume() {
    	ActionBar actionBar = this.getSupportActionBar();
    	actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.orange)));
    	super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

	@Override
	public void loadFragments(Fragment fragment, Boolean addToBackStack) {
		this.getSupportFragmentManager()
		.beginTransaction()
        .replace(R.id.container, fragment)
        .addToBackStack("sessionMapFragment")
        .commit();
	}

}
